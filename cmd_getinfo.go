package asaur

import (
	"fmt"
	"strings"
)

// GetStatus returns tor control port info (ex: "status/bootstrap-phase")
func (c *Conn) GetInfo(arg string) ([]string, error) {
	cmd := fmt.Sprintf("GETINFO %v", arg)
	resp, err := c.Request(cmd)
	if err != nil {
		return nil, err
	}
	return resp.Data, nil
}

func GetInfo(torControlAddress string, torControlSocketType string, authentication string, arg string) ([]string, error) {
	c, err := Dial(torControlSocketType, torControlAddress)
	if err != nil {
		return nil, err
	}
	defer c.Close()

	if err := c.Authenticate(authentication); err != nil {
		return nil, err
	}

	return c.GetInfo(arg)
}

func ParseBootstrapPhase(data []string) map[string]string {
	// TODO: Fix "5" which is status/bootstrap-phase specific hack to avoid "" parsing
	lines := strings.SplitN(data[0], " ", 5)
	var resps = make(map[string]string)
	for _, l := range lines {
		kv := strings.Split(l, "=")
		if len(kv) == 2 {
			kv[1] = strings.TrimPrefix(kv[1], "\"")
			kv[1] = strings.TrimSuffix(kv[1], "\"")
			resps[kv[0]] = kv[1]
		}
	}
	return resps
}
